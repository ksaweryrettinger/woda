#include "cModbusMaster.h"

cModbusMaster::cModbusMaster(cEntry* pEntry, cMain* pMain)
{
    this->pMain = pMain;

    //Copy Modbus RTU parameters
	this->mbAddress = pEntry->mbAddress;
	this->mbPort = pEntry->mbPort;

	//Copy leak detection parameters
	fLeakDetectCFG[0] = pEntry->fLeakDetectCFG[0];
	fLeakDetectCFG[1] = pEntry->fLeakDetectCFG[1];
	fLeakDetect[0] = pEntry->fLeakDetectCFG[0];
	fLeakDetect[1] = pEntry->fLeakDetectCFG[1];

	//Initialize errors
	bModbusError = false;

	//Initialize readings
	ExpanderU6 = 0;
	ExpanderU7 = 0;
	WaterLevelAlerts = 0;
	bEmergencyStop = false;

	//Initialize pointers
    ThreadTCP = NULL;
    ctxRTU = NULL;

    //Other
    bStartMain = false;

    //Create Modbus Mapping (TCP/IP Server)
    mb_mapping = modbus_mapping_new_start_address(1, MB_NUM_COILS, 10001, MB_NUM_DISCRETE_INPUTS,
    		40001, MB_NUM_HOLDING_REGISTERS, 30001, MB_NUM_INPUT_REGISTERS + 2);

    //Start main (Modbus RTU) thread
    ThreadReadRTU = new cThreadReadRTU(this);
    ThreadReadRTU->Run();

	//Start TCP thread
	ThreadTCP = new cThreadTCP(this);
	ThreadTCP->Run();

	pMain->mbMaster = this;
}

//Send emergency stop water command to controller
void cModbusMaster::WaterEmergencyStop(bool bEmergencyStop)
{
	uint8_t mbDataNum = 1;
	uint16_t mbData = (uint16_t) bEmergencyStop;

	//Update Modbus RTU Coil
	ThreadWriteRTU[MB_THREAD_WRITE_COIL] = new cThreadWriteRTU(this, &mbData, mbDataNum, 1, true);
	ThreadWriteRTU[MB_THREAD_WRITE_COIL]->Run();
}

//Send emergency stop water command to controller
void cModbusMaster::ResetLeak(void)
{
	uint8_t mbDataNum = 1;
	uint16_t mbData = 1;

	//Update Modbus RTU Coil
	ThreadWriteRTU[MB_THREAD_WRITE_COIL] = new cThreadWriteRTU(this, &mbData, mbDataNum, 2, true);
	ThreadWriteRTU[MB_THREAD_WRITE_COIL]->Run();
}


//Send emergency stop water command to controller
void cModbusMaster::SetLeakParameters(float* fNewSettings)
{
	if (!ThreadWriteRTU[MB_THREAD_WRITE_HOLDING])
	{
		wxString strBCD;
		uint16_t mbNewSettingsBCD[MB_NUM_HOLDING_REGISTERS] = { 0 };

		for (uint8_t i = 0; i < MB_NUM_HOLDING_REGISTERS; i++)
		{
			//Convert new settings from float to BCD
			strBCD = wxString::Format(wxT("%.2f"), fNewSettings[i]);

			if (fNewSettings[i] < 10)
			{
				mbNewSettingsBCD[i] = 0;
				mbNewSettingsBCD[i] |= (strBCD[0] - '0') << 8;
				mbNewSettingsBCD[i] |= (strBCD[2] - '0') << 4;
				mbNewSettingsBCD[i] |= (strBCD[3] - '0');
			}
			else
			{
				mbNewSettingsBCD[i] = (strBCD[0] - '0') << 12;
				mbNewSettingsBCD[i] |= (strBCD[1] - '0') << 8;
				mbNewSettingsBCD[i] |= (strBCD[3] - '0') << 4;
				mbNewSettingsBCD[i] |= (strBCD[4] - '0');
			}
		}

		//Update Modbus RTU Holding Registers
		ThreadWriteRTU[MB_THREAD_WRITE_HOLDING] = new cThreadWriteRTU(this, mbNewSettingsBCD, MB_NUM_HOLDING_REGISTERS, 40001, false);
		ThreadWriteRTU[MB_THREAD_WRITE_HOLDING]->Run();
	}
}

//Close all connections and terminate active Modbus threads
void cModbusMaster::CloseModbus(void)
{
	//Terminate RTU write threads
	while (ThreadWriteRTU[MB_THREAD_WRITE_COIL] != NULL) wxMilliSleep(1);
	while (ThreadWriteRTU[MB_THREAD_WRITE_HOLDING] != NULL) wxMilliSleep(1);

	//Terminate TCP/IP thread
	if (ThreadTCP) ThreadTCP->Delete();
	while (ThreadTCP != NULL) wxMilliSleep(1);

	//Terminate RTU read thread
	if (ThreadReadRTU != NULL) ThreadReadRTU->Delete();
    while (ThreadReadRTU != NULL) wxMilliSleep(1);
}
