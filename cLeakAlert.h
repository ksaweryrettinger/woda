#pragma once

#include "wx/wx.h"
#include "cMain.h"

class cMain;

class cLeakAlert : public wxFrame
{
public: //constructor
	cLeakAlert(cMain*);
	~cLeakAlert();

private: //pointer received in contructor
	cMain* pMain;
	void OnButtonConfirm(wxCommandEvent& evt);
	void OnSoundAlert(wxTimerEvent& evt);

private: //variables and pointers
    wxStaticBitmap* imgWarning;
    wxStaticText* txtMessage;
	wxButton* btnConfirm;
	wxButton* btnCancel;
	wxTimer* tAlertTimer;

	friend class cMain;

	wxDECLARE_EVENT_TABLE();
};
