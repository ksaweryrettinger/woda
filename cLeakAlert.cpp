#include "cLeakAlert.h"

wxBEGIN_EVENT_TABLE(cLeakAlert, wxFrame)
	EVT_BUTTON(10006, cLeakAlert::OnButtonConfirm)
	EVT_TIMER(20003, cLeakAlert::OnSoundAlert)
wxEND_EVENT_TABLE()

cLeakAlert::cLeakAlert(cMain* pMain) : wxFrame(nullptr, wxID_ANY, _T("Obieg Wewnętrzny"), wxPoint(390, 300), wxSize(449, 182), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX & ~wxCLOSE_BOX))
{
    this->pMain = pMain;

	//Set window background colour
	this->SetBackgroundColour(wxColour(225, 225, 225));

	//Create images
	imgWarning = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/Error.png"), wxBITMAP_TYPE_PNG), wxPoint(30, 27), wxSize(100, 100));

	//Create static text
	txtMessage = new wxStaticText(this, wxID_ANY, _T("Nieszczelność w\nObiegu Wewnętrznym!"), wxPoint(63, 35), wxSize(450, 50), wxALIGN_CENTRE_HORIZONTAL);
	txtMessage->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_HEAVY));
	txtMessage->SetForegroundColour(wxColour(207, 31, 37));

    //Create buttons
    btnCancel = new wxButton(this, 10005, _T("Anuluj"), wxPoint(165, 88), wxSize(110, 35));
    btnCancel->SetBackgroundColour(wxColour(210, 210, 210));
    btnCancel->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_MEDIUM));
    btnCancel->Disable();
    btnConfirm = new wxButton(this, 10006, _T("Potwierdź"), wxPoint(294, 88), wxSize(110, 35));
    btnConfirm->SetBackgroundColour(wxColour(210, 210, 210));
    btnConfirm->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_MEDIUM));

    //Play sound alert once
	wxExecute("aplay " + wxString::FromUTF8(getenv("HOME")) + "/eclipse-workspace/Woda/Sounds/Chime.wav");

	//Alert sound timer
    tAlertTimer = new wxTimer(this, 20003);
    tAlertTimer->Start(DELAY_ALERT_SOUND);
}

void cLeakAlert::OnSoundAlert(wxTimerEvent& evt)
{
	wxExecute("aplay " + wxString::FromUTF8(getenv("HOME")) + "/eclipse-workspace/Woda/Sounds/Chime.wav");
}

void cLeakAlert::OnButtonConfirm(wxCommandEvent& evt)
{
	//Send command to reset leak alerts
	pMain->mbMaster->ResetLeak();
	pMain->bLeakReset = true;

	evt.Skip();
}

cLeakAlert::~cLeakAlert()
{
	tAlertTimer->Stop();
	pMain->pLeakAlert = NULL;
}
