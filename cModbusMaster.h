#pragma once

#include "wParameters.h"
#include "cEntry.h"
#include "cMain.h"
#include "cModbusRTU.h"
#include "cThreadTCP.h"

class cEntry;
class cMain;
class cMainError;
class cThreadReadRTU;
class cThreadWriteRTU;
class cThreadTCP;

/* Modbus Gateway class */
class cModbusMaster
{
	public:
		cModbusMaster(cEntry*, cMain*);
		void CloseModbus(void);
		void WaterEmergencyStop(bool);
		void SetLeakParameters(float*);
		void ResetLeak(void);

	public: //class pointers and flags
		cMain* pMain;

	public: //modbus
		int32_t mbAddress;
		wxString mbPort;
		modbus_t* ctxRTU;
		modbus_mapping_t* mb_mapping;

	public: //critical sections
		wxCriticalSection mb_guard; //prevent simultaneous Modbus RTU operations
		wxCriticalSection local_guard; //prevent simultaneous local data access
		wxCriticalSection remote_guard; //prevent simultaneous remote data access

	public: //threads
		cThreadTCP* ThreadTCP;
		cThreadReadRTU* ThreadReadRTU;
		cThreadWriteRTU* ThreadWriteRTU[2] = { NULL };

	public: //SHARED DATA

		//Show main window flag
		bool bStartMain;

		//Leak detection
		float fLeakDetect[2] = { 0 };

		//Leak detection (configuration file)
		float fLeakDetectCFG[2] = { 0 };

		//Emergency water stop
		bool bEmergencyStop;

		//Errors
		bool bModbusError;
		bool bTempSDError[NUM_TEMP_SENSORS] = { false };
		bool bTempTimeoutError[NUM_TEMP_SENSORS] = { false };
		bool bI2CError[NUM_DEVICES_I2C] = { false };

		//Readings
		uint8_t ExpanderU6;
		uint8_t ExpanderU7;
		uint16_t Temperature[NUM_TEMP_SENSORS] = { 0 };
		uint16_t WaterLevelInner[NUM_WATER_READINGS] = { 0 };
		uint16_t WaterLevelOuter[NUM_WATER_READINGS] = { 0 };
		uint16_t WaterLevelAlerts;

		//Coils and registers
		uint16_t mbCoils[MB_NUM_COILS] = { 0 };
		uint16_t mbHoldingRegisters[MB_NUM_HOLDING_REGISTERS] = { 0 };
};
