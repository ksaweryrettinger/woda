#pragma once

#include "wx/wx.h"
#include "cMain.h"

class cMain;

class cLeakParameters : public wxFrame
{
public: //constructor
	cLeakParameters(cMain*);

private: //event handlers
	void OnClose(wxCloseEvent& evt);

private: //pointer received in contructor
	cMain* pMain;
	void OnParametersChanged(wxCommandEvent& evt);

private: //variables and pointers
    wxFont myFont;
    wxTextCtrl* txtCtrlSmallLeak;
    wxStaticText* txtTitleSmallLeak;
	wxStaticText* txtUnitsSmallLeak;
    wxTextCtrl* txtCtrlLargeLeak;
    wxStaticText* txtTitleLargeLeak;
	wxStaticText* txtUnitsLargeLeak;
	wxButton* btnConfirmSettings;

	wxDECLARE_EVENT_TABLE();
};

