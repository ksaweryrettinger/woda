#include "cEntry.h"

wxIMPLEMENT_APP(cEntry);

cEntry::cEntry()
{
	//Initialize variables and pointers
    pMain = NULL;
	mbMaster = NULL;
    wxChecker = NULL;
    bConfigError = false;
    mbAddress = 0;
}

bool cEntry::OnInit()
{
    wxApp::SetAppName(wxString(_T("Woda")));

    {
        wxLogNull logNo; //suppress information message
        wxChecker = new wxSingleInstanceChecker(_T(".woda_temp"));
    }

    //Check if process already exists
    if (wxChecker->IsAnotherRunning())
    {
		if (wxMessageBox(_T("\nProces już istnieje.\nZakończyć poprzedni proces?"),
				_T("Woda"), wxOK | wxCANCEL | wxICON_INFORMATION) == wxOK)
		{
			//Get PID of previous instance
	        string command = "kill $(pidof -o " + to_string(::getpid()) + " Woda)";
	        int32_t rtn = system(command.c_str());

	        //Delete old single instance checker
	        command = "rm " + wxString::FromUTF8(getenv("HOME")).ToStdString() + "/.woda_temp";
	        rtn = system(command.c_str());
	        rtn = rtn + 1; //to prevent compiler warnings

	        //Create new single instance checker
	        wxLogNull logNo; //suppress information message
			delete wxChecker;
	        wxChecker = new wxSingleInstanceChecker(_T(".woda_temp"));
		}
		else
		{
			delete wxChecker;
			wxChecker = NULL;
			return false;
		}
    }

	//Open configuration file
	filename = wxString::FromUTF8(getenv("HOME")) + "/eclipse-workspace/Woda/woda.cnf";
	tConfigFile.Open(filename);

	string s;
	vector<string> v;

	while (!tConfigFile.Eof())
	{
		strConfig = tConfigFile.GetNextLine();

		if ((strConfig[0] != '#') && !(strConfig.IsEmpty()))
		{
			s = strConfig.ToStdString();
			boost::trim_if(s, boost::is_any_of("\t "));
			boost::split(v, s, boost::is_any_of(" \t"), boost::token_compress_on);

			if (v[0] == "MBS") //Modbus Slave Address
			{
				if (v.size() >= 2) mbAddress = wxAtoi(v[1]);
				else
				{
					bConfigError = true;
					break;
				}
			}
			else if (v[0] == "TTY") //Linux Serial Port
			{
				if (v.size() >= 2) mbPort = v[1];
				else
				{
					bConfigError = true;
					break;
				}
			}
			else if (v[0] == "WYC") //Leak Detection Parameters
			{
				if (v.size() >= 3) fLeakDetectCFG[wxAtoi(v[1])] = wxAtof(v[2]);
				else
				{
					bConfigError = true;
					break;
				}
			}
		}
	}

	if (bConfigError)
	{
		 wxLogError(_T("\n       Błąd w pliku konfiguracyjnym!      "));
		 return false;
	}

    //Create Main window
    pMain = new cMain(this);
	pMain->SetIcon(wxICON(icon));

	//Start Modbus master
	mbMaster = new cModbusMaster(this, pMain);

	//Wait for new data
	while (!mbMaster->bStartMain) {};
	wxMilliSleep(1000);

	//Show main window after delay
	pMain->Show();

	return true;
}

int cEntry::OnExit()
{
    //Close Modbus gateway and free resources
    mbMaster->CloseModbus();

    //Free pointers
    delete mbMaster;
    delete wxChecker;
    mbMaster = NULL;
    wxChecker = NULL;

    return 0;
}
