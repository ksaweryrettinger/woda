#include "cThreadTCP.h"

/**************************************** Modbus TCP/IP Thread **********************************************/

cThreadTCP::cThreadTCP(cModbusMaster* mbMaster) : wxThread(wxTHREAD_DETACHED)
{
	this->mbMaster = mbMaster;
	tcpQuery = (uint8_t*)malloc(MODBUS_TCP_MAX_ADU_LENGTH);
    rc = -1;
    rp = -1;
    ct = -1;
	regNum = 0;
	regStart = 0;
    regHoldingTemp = 0;

	//Create new TCP/IP configuration
    ctxTCP = modbus_new_tcp_pi(NULL, "1502");
    modbus_set_indication_timeout(ctxTCP, 0, MB_INDICATION_TIMEOUT_US);
    RestartSocketTCP(ctxTCP);
}

cThreadTCP::~cThreadTCP()
{
	mbMaster->ThreadTCP = NULL;
	if (socket != -1) close(socket);
    free(tcpQuery);
}

wxThread::ExitCode cThreadTCP::Entry()
{
	while (!TestDestroy())
	{
		if (ct == -1) //connection NOK
		{
			ct = modbus_tcp_pi_accept(ctxTCP, &socket); //wait for new connections (NON-BLOCKING)
			wxThread::Sleep(DELAY_CONNECTION_TCP);
		}
		else //connection OK
		{
			rc = modbus_receive(ctxTCP, tcpQuery); //wait for new indication (BLOCKING, WITH TIMEOUT)

			if (rc != -1) //process client request and send reply
			{
				wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);

				//Send reply to client
				rp = modbus_reply(ctxTCP, tcpQuery, rc, mbMaster->mb_mapping); //send reply

				if (rp != -1) //no reply errors
				{
					if (mbMaster->pMain != NULL)
					{
						if (mbMaster->pMain->eUserMode == Remote) //accept commands only in REMOTE mode
						{
							//Check for new commands
							if (mbMaster->mb_mapping->last_indication.function == MODBUS_FC_WRITE_SINGLE_COIL ||
								mbMaster->mb_mapping->last_indication.function == MODBUS_FC_WRITE_MULTIPLE_COILS)
							{
								regStart = mbMaster->mb_mapping->last_indication.start_address - mbMaster->mb_mapping->start_bits;
								regNum = mbMaster->mb_mapping->last_indication.numreg;

								for (int32_t regID = regStart; regID < (regStart + regNum); regID++)
								{
									if (regID == 0) mbMaster->WaterEmergencyStop(mbMaster->mb_mapping->tab_bits[regID] == 1);
									if (regID == 1) mbMaster->ResetLeak();

								}
							}
							else if (mbMaster->mb_mapping->last_indication.function == MODBUS_FC_WRITE_SINGLE_REGISTER ||
								mbMaster->mb_mapping->last_indication.function == MODBUS_FC_WRITE_MULTIPLE_REGISTERS)
							{
								{
									wxCriticalSectionLocker local_lock(mbMaster->local_guard);
									fNewSettings[0] = mbMaster->fLeakDetect[0];
									fNewSettings[1] = mbMaster->fLeakDetect[1];
								}

								//New water leak settings
								regStart = mbMaster->mb_mapping->last_indication.start_address - mbMaster->mb_mapping->start_registers;
								regNum = mbMaster->mb_mapping->last_indication.numreg;

								for (int32_t regID = regStart; regID < (regStart + regNum); regID++)
								{
									//Copy holding register
									regHoldingTemp = mbMaster->mb_mapping->tab_registers[regID];

									//Convert setting from BCD to float
									cNewSetting[0] = ((regHoldingTemp >> 12) & 0xF) + '0';
									cNewSetting[1] = ((regHoldingTemp >> 8) & 0xF) + '0';
									cNewSetting[2] = '.';
									cNewSetting[3] = ((regHoldingTemp >> 4) & 0xF) + '0';
									cNewSetting[4] = (regHoldingTemp & 0xF) + '0';
									cNewSetting[5] = '\n';

									fNewSettings[regID] = atof(cNewSetting);
								}

								mbMaster->SetLeakParameters(fNewSettings);
							}
						}
					}
				}
				else if (errno != ENOPROTOOPT) //REPLY ERROR
				{
					RestartSocketTCP(ctxTCP);
				}
			}
			else if (errno != EMBBADDATA && errno != ETIMEDOUT) //RECEIVE ERROR
			{
				RestartSocketTCP(ctxTCP); //connection closed by client or error
			}
		}
	}

	return (wxThread::ExitCode) 0;
}

void cThreadTCP::RestartSocketTCP(modbus_t* ctxTCP)
{
	ct = -1;
	if (socket != -1) close(socket);
    socket = modbus_tcp_pi_listen(ctxTCP, 1); //create new socket
    fcntl(socket, F_SETFL, fcntl(socket, F_GETFL, 0) | O_NONBLOCK); //non-blocking
}
