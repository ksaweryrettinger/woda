#include "cUserMessage.h"

wxBEGIN_EVENT_TABLE(cUserMessage, wxFrame)
	EVT_CLOSE(cUserMessage::OnClose)
	EVT_BUTTON(10003, cUserMessage::OnButtonCancel)
	EVT_BUTTON(10004, cUserMessage::OnButtonConfirm)
	EVT_TIMER(20002, cUserMessage::EmergencyStopTimeout)
wxEND_EVENT_TABLE()

cUserMessage::cUserMessage(cMain* pMain) : wxFrame(nullptr, wxID_ANY, _T("Awaryjne Wyłączenie Obiegu"), wxPoint(390, 300), wxSize(449, 182), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX))
{
    this->pMain = pMain;

	//Set window background colour
	this->SetBackgroundColour(wxColour(225, 225, 225));

	//Create images
	imgWarning = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/Warning.png"), wxBITMAP_TYPE_PNG), wxPoint(30, 27), wxSize(100, 100));

	imgError = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/Error.png"), wxBITMAP_TYPE_PNG), wxPoint(38, 27), wxSize(100, 100));
	imgError->Hide();

	//Create static text
    txtMessage = new wxStaticText(this, wxID_ANY, _T("Wyłączyć Obieg Wewnętrzny?"), wxPoint(60, 43), wxSize(450, 30), wxALIGN_CENTRE_HORIZONTAL);
    txtMessage->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_HEAVY));
    txtMessage->SetForegroundColour(wxColour());

    //Create buttons
    btnCancel = new wxButton(this, 10003, _T("Anuluj"), wxPoint(165, 83), wxSize(110, 35));
    btnCancel->SetBackgroundColour(wxColour(210, 210, 210));
    btnCancel->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_MEDIUM));
    btnConfirm = new wxButton(this, 10004, _T("Potwierdź"), wxPoint(294, 83), wxSize(110, 35));
    btnConfirm->SetBackgroundColour(wxColour(210, 210, 210));
    btnConfirm->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_MEDIUM));

	//TIMEOUT TIMER
	tEmergencyStopTimer = new wxTimer(this, 20002);
}

void cUserMessage::OnButtonCancel(wxCommandEvent& evt)
{
	pMain->pUserMessage = NULL;
	Destroy();
}

void cUserMessage::OnButtonConfirm(wxCommandEvent& evt)
{
	if (imgWarning->IsShown())
	{
		//Send emergency stop command
		pMain->mbMaster->WaterEmergencyStop(true);
		tEmergencyStopTimer->Start(EMERGENCY_STOP_TIMEOUT, true);
		this->Hide();
		btnCancel->Disable();
	}
	else
	{
		//Close window
		pMain->pUserMessage = NULL;
		Destroy();
	}

	evt.Skip();
}

void cUserMessage::EmergencyStopTimeout(wxTimerEvent& evt)
{
	//Update images and user message
	imgWarning->Hide();
	imgError->Show();
	txtMessage->SetLabel(_T("Błąd w Wyłączaniu Obiegu!"));
	txtMessage->SetForegroundColour(wxColour(207, 31, 37));

	//Display updated window
	btnConfirm->Refresh();
	btnConfirm->Update();
	this->Show();
}

void cUserMessage::OnClose(wxCloseEvent& evt)
{
	//Close window
	if (tEmergencyStopTimer->IsRunning()) tEmergencyStopTimer->Stop();
	pMain->pUserMessage = NULL;
	Destroy();
}
