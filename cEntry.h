#pragma once

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wwrite-strings"

#include "wParameters.h"
#include "Images/Icon.xpm"
#include "cMain.h"
#include "cModbusMaster.h"

class cMain;
class cModbusMaster;

class cEntry : public wxApp
{
public:
	cEntry();

public:
    virtual bool OnInit() override;
    virtual int OnExit() override;

public: //class pointers
    cMain* pMain;
    cModbusMaster* mbMaster;
    wxSingleInstanceChecker* wxChecker;

public: //Water leak detection
    float fLeakDetectCFG[2] = { 0 };

private: //Modbus configuration
    int32_t mbAddress;
    wxString mbPort;

private: //Configuration file variables
    wxTextFile tConfigFile;
    wxString filename;
    wxString strConfig;
    bool bConfigError;

    friend class cModbusMaster;
};

#pragma GCC diagnostic pop
