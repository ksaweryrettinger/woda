#include "cLeakParameters.h"

wxBEGIN_EVENT_TABLE(cLeakParameters, wxFrame)
	EVT_CLOSE(cLeakParameters::OnClose)
	EVT_BUTTON(10002, cLeakParameters::OnParametersChanged)
wxEND_EVENT_TABLE()

cLeakParameters::cLeakParameters(cMain* pMain) : wxFrame(nullptr, wxID_ANY, _T("Parametry Wykrywania Nieszczelności"), wxPoint(390, 300), wxSize(445, 167), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX))
{
    this->pMain = pMain;

	//Set window background colour
	this->SetBackgroundColour(wxColour(225, 225, 225));

	//Initialize static text
    txtCtrlSmallLeak = new wxTextCtrl(this, wxID_ANY, "", wxPoint(170, 29), wxSize(80, 31), wxALIGN_CENTRE_HORIZONTAL);
    txtTitleSmallLeak = new wxStaticText(this, wxID_ANY, _T("Mała Nieszczelność:"), wxPoint(15, 35), wxSize(150, 30), wxALIGN_CENTRE_HORIZONTAL);
    txtUnitsSmallLeak = new wxStaticText(this, wxID_ANY, _T("\x2030/min"), wxPoint(262, 36), wxSize(55, 20), wxALIGN_LEFT);

    txtCtrlLargeLeak = new wxTextCtrl(this, wxID_ANY, "", wxPoint(170, 79), wxSize(80, 31), wxALIGN_CENTRE_HORIZONTAL);
    txtTitleLargeLeak = new wxStaticText(this, wxID_ANY, _T("Duża Nieszczelność:"), wxPoint(15, 85), wxSize(150, 40), wxALIGN_CENTRE_HORIZONTAL);
    txtUnitsLargeLeak = new wxStaticText(this, wxID_ANY, _T("\x2030/min"), wxPoint(262, 86), wxSize(55, 20), wxALIGN_LEFT);

    //Initialize button
    btnConfirmSettings = new wxButton(this, 10002, _T("Potwierdź"), wxPoint(331, 54), wxSize(85, 30));
    btnConfirmSettings->SetBackgroundColour(wxColour(210, 210, 210));

    //Initialize values in window to current settings
    if (pMain->fLeakDetect[0] >= 0) txtCtrlSmallLeak->SetValue(wxString::Format(wxT("%.2f"), pMain->fLeakDetect[0]));
    if (pMain->fLeakDetect[1] >= 0) txtCtrlLargeLeak->SetValue(wxString::Format(wxT("%.2f"), pMain->fLeakDetect[1]));
}

void cLeakParameters::OnParametersChanged(wxCommandEvent& evt)
{
	float fNewSettings[NUM_LEAK_DETECT] = { 0 };

	/*********************************** Small Leaks ************************************/

	//Get new value
	wxString strNewSetting = txtCtrlSmallLeak->GetValue();
	fNewSettings[0] = wxAtof(strNewSetting);

	//Update display
	if (fNewSettings[0] < MIN_LEAK) fNewSettings[0] = MIN_LEAK;
	else if (fNewSettings[0] > MAX_LEAK) fNewSettings[0] = MAX_LEAK;
	txtCtrlSmallLeak->SetValue(wxString::Format(wxT("%.2f"), fNewSettings[0]));

	/*********************************** Large Leaks ************************************/

	//Get new value
	strNewSetting = txtCtrlLargeLeak->GetValue();
	fNewSettings[1] = wxAtof(strNewSetting);

	//Update display
	if (fNewSettings[1] < MIN_LEAK) fNewSettings[1] = MIN_LEAK;
	else if (fNewSettings[1] > MAX_LEAK) fNewSettings[1] = MAX_LEAK;
	txtCtrlLargeLeak->SetValue(wxString::Format(wxT("%.2f"), fNewSettings[1]));

	/******************************* Send New Parameters ********************************/

	pMain->mbMaster->SetLeakParameters(fNewSettings);
}

/* Constants frame closed */
void cLeakParameters::OnClose(wxCloseEvent& evt)
{
	pMain->pLeakParameters = NULL;
	Destroy();
}
