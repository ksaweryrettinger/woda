#include "cModbusRTU.h"

/****************************************  Thread **********************************************/

cThreadReadRTU::cThreadReadRTU(cModbusMaster* mbMaster) : wxThread(wxTHREAD_DETACHED)
{
	//Constructor variables
	this->mbMaster = mbMaster;

	//Initialize variables
    rc = -1;

	//Create new RTU configuration
    mbMaster->ctxRTU = modbus_new_rtu(mbMaster->mbPort.mb_str(), MB_BAUD, 'E', 8, 1);
	modbus_set_slave(mbMaster->ctxRTU, mbMaster->mbAddress);
	modbus_rtu_set_serial_mode(mbMaster->ctxRTU, MODBUS_RTU_RS232);
	modbus_set_response_timeout(mbMaster->ctxRTU, MB_RESPONSE_TIMEOUT_SEC, MB_RESPONSE_TIMEOUT_US);
	ct = modbus_connect(mbMaster->ctxRTU);
	bModbusError = (bool) (ct == -1);

	/************************************ Local Data *******************************************/

	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);
		mbMaster->bModbusError = bModbusError;
	}

	/************************************ Start Main (Error Mode) ******************************/

	//Modbus connection error
	if (bModbusError)
    {
    	//Signal program to start main window
    	mbMaster->bStartMain = true;

    	/************************************ Remote Data ******************************************/

    	{
    		wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);
    		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_RTU_ERROR - 1] = (uint16_t) bModbusError;
    	}
    }
}

wxThread::ExitCode cThreadReadRTU::Entry()
{
	if (!bModbusError)
	{
		/**************************** Leak Detection Initialization  ********************************/

		mbMaster->SetLeakParameters(mbMaster->fLeakDetectCFG);

		/************************************ Main RTU Loop *****************************************/

		while (!TestDestroy())
		{
			bModbusError = false;

			/************************************* Read Coils *******************************************/

			{
				wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
				rc = modbus_read_bits(mbMaster->ctxRTU, 1, MB_NUM_COILS, mbCoils);
				if (rc == -1) bModbusError = true;
			}

			/******************************** Read Inputs Registers *************************************/

			{
				wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
				rc = modbus_read_input_registers(mbMaster->ctxRTU, 30001, MB_NUM_INPUT_REGISTERS, mbInputRegisters);
				if (rc == -1) bModbusError = true;
			}

			/******************************** Read Holding Registers ************************************/

			{
				wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
				rc = modbus_read_registers(mbMaster->ctxRTU, 40001, MB_NUM_HOLDING_REGISTERS, mbHoldingRegisters);
				if (rc == -1) bModbusError = true;
			}

			//Update shared data in Modbus master
			UpdateModbusData();

			//Signal program to start main window
			if (!mbMaster->bStartMain) mbMaster->bStartMain = true;

			//Delay next thread iteration
			wxThread::Sleep(DELAY_MODBUS_RTU);
		}
	}

	return (wxThread::ExitCode) 0;
}

void cThreadReadRTU::UpdateModbusData(void)
{
	/************************************ Local Data *******************************************/

	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);

		/*********************************** Modbus Errors *****************************************/

		mbMaster->bModbusError = bModbusError;

		/******************************** Temperature Sensors **************************************/

		for (uint8_t i = 0; i < NUM_TEMP_SENSORS; i++)
		{
			mbMaster->Temperature[i] = mbInputRegisters[MB_INPUTREG_TEMP_START - 1 + i];
			mbMaster->bTempSDError[i] = (bool) ((mbInputRegisters[MB_INPUTREG_TEMP_SD - 1] >> i) & 1);
			mbMaster->bTempTimeoutError[i] = (bool) ((mbInputRegisters[MB_INPUTREG_TEMP_TIMEOUT - 1] >> i) & 1);
		}

		/************************************ I2C Errors *******************************************/

		for (uint8_t i = 0; i < NUM_DEVICES_I2C; i++)
		{
			mbMaster->bI2CError[i] = (bool) ((mbInputRegisters[MB_INPUTREG_I2C_ERRORS - 1] >> i) & 1);
		}

		/*********************************** Water Levels ******************************************/

		for (uint8_t i = 0; i < NUM_WATER_READINGS; i++)
		{
			mbMaster->WaterLevelInner[i] = mbInputRegisters[MB_INPUTREG_WATERIN_START - 1 + i];
			mbMaster->WaterLevelOuter[i] = mbInputRegisters[MB_INPUTREG_WATEROUT_START - 1 + i];
		}

		/****************************** Emergency Stop Status **************************************/

		mbMaster->bEmergencyStop = (bool) (mbCoils[0] == 1);

		/************************* Expanders and Water Level Alerts ********************************/

		mbMaster->WaterLevelAlerts = mbInputRegisters[MB_INPUTREG_WATER_ALERTS - 1];
		mbMaster->ExpanderU6 = (uint8_t) mbInputRegisters[MB_INPUTREG_EXPANDER_U6 - 1];
		mbMaster->ExpanderU7 = (uint8_t) mbInputRegisters[MB_INPUTREG_EXPANDER_U7 - 1];

		/**************************** Leak Detection Parameters ************************************/

		for (uint8_t i = 0; i < NUM_LEAK_DETECT; i++)
		{
			//Convert setting from BCD to float
			cNewSetting[0] = ((mbHoldingRegisters[i] >> 12) & 0xF) + '0';
			cNewSetting[1] = ((mbHoldingRegisters[i] >> 8) & 0xF) + '0';
			cNewSetting[2] = '.';
			cNewSetting[3] = ((mbHoldingRegisters[i] >> 4) & 0xF) + '0';
			cNewSetting[4] = (mbHoldingRegisters[i] & 0xF) + '0';
			cNewSetting[5] = '\n';

			mbMaster->fLeakDetect[i] = atof(cNewSetting);
		}
	}

	/************************************ Remote Data ******************************************/

	{
		wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);

		//Modbus RTU data
		for (uint8_t i = 0; i < MB_NUM_COILS; i++) mbMaster->mb_mapping->tab_bits[i] = mbCoils[i];
		for (uint8_t i = 0; i < MB_NUM_INPUT_REGISTERS; i++) mbMaster->mb_mapping->tab_input_registers[i] = mbInputRegisters[i];
		for (uint8_t i = 0; i < MB_NUM_HOLDING_REGISTERS; i++) mbMaster->mb_mapping->tab_registers[i] = mbHoldingRegisters[i];

		//Modbus RTU errors
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_RTU_ERROR - 1] = (uint16_t) bModbusError;
	}
}

cThreadReadRTU::~cThreadReadRTU()
{
	wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
	modbus_close(mbMaster->ctxRTU);
	modbus_free(mbMaster->ctxRTU);
	mbMaster->ThreadReadRTU = NULL;
}

/**************************************** Set-Current Thread ***********************************************/

cThreadWriteRTU::cThreadWriteRTU(cModbusMaster *mbMaster, uint16_t* mbData, uint8_t mbDataNum, int32_t mbStartAddress, bool bSetCoil) : wxThread(wxTHREAD_DETACHED)
{
	//Copy constructor data
    this->mbMaster = mbMaster;

    //Copy Modbus data
    for (uint8_t i = 0; i < mbDataNum; i++) this->mbData[i] = mbData[i];

    //Copy remaining data
	this->mbDataNum = mbDataNum;
	this->mbStartAddress = mbStartAddress;
	this->bSetCoil = bSetCoil;
    rc = -1;

    {
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);
		bModbusError = mbMaster->bModbusError;
    }
}

wxThread::ExitCode cThreadWriteRTU::Entry()
{
	if (!bModbusError)
	{
		wxCriticalSectionLocker mb_lock(mbMaster->mb_guard);
		if (bSetCoil) rc = modbus_write_bits(mbMaster->ctxRTU, mbStartAddress, mbDataNum, (uint8_t*) mbData); //write to coils
		else rc = modbus_write_registers(mbMaster->ctxRTU, mbStartAddress, mbDataNum, mbData); //write to holding registers
	}

	//Terminate thread
	return (wxThread::ExitCode) 0;
}

cThreadWriteRTU::~cThreadWriteRTU()
{
	if (bSetCoil) mbMaster->ThreadWriteRTU[MB_THREAD_WRITE_COIL] = NULL;
	else mbMaster->ThreadWriteRTU[MB_THREAD_WRITE_HOLDING] = NULL;
}
