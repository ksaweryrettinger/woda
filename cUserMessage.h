#pragma once

#include "wx/wx.h"
#include "cMain.h"

class cMain;

class cUserMessage : public wxFrame
{
public: //constructor
	cUserMessage(cMain*);

private: //event handlers
	void OnClose(wxCloseEvent& evt);

private: //pointer received in contructor
	cMain* pMain;
	void OnButtonConfirm(wxCommandEvent& evt);
	void OnButtonCancel(wxCommandEvent& evt);
	void EmergencyStopTimeout(wxTimerEvent& evt);

private: //variables and pointers
    wxStaticBitmap* imgWarning;
    wxStaticBitmap* imgError;
    wxStaticText* txtMessage;
	wxButton* btnConfirm;
	wxButton* btnCancel;
	wxTimer* tEmergencyStopTimer;

	friend class cMain;

	wxDECLARE_EVENT_TABLE();
};
