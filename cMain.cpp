#include "cMain.h"

wxBEGIN_EVENT_TABLE(cMain, wxFrame)
	EVT_MENU(wxID_INFO, cMain::OnMenuClickedInformation)
	EVT_MENU(wxID_PREFERENCES, cMain::OnMenuClickedParameters)
	EVT_MENU(wxID_NETWORK, cMain::OnMenuClickedMode)
	EVT_BUTTON(10001, cMain::OnButtonEmergencyStop)
	EVT_TIMER(20001, cMain::OnRefreshDisplay)
    EVT_CLOSE(cMain::OnClose)
wxEND_EVENT_TABLE()

/* Frame constructor */
cMain::cMain(cEntry* pEntry) : wxFrame(nullptr, wxID_ANY, " ", wxPoint(200, 150), wxSize(909, 700), (wxDEFAULT_FRAME_STYLE & ~wxRESIZE_BORDER & ~wxMAXIMIZE_BOX))
{
    //FRAME TITLE AND COLOUR
    this->SetTitle(_T("Woda"));
    this->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
    this->SetBackgroundColour(wxColour(225, 225, 225));

    //CONSTRUCTOR VARIABLES
    mbMaster = NULL;
    pLeakParameters = NULL;
    pUserMessage = NULL;
    pLeakAlert = NULL;
    pInformation = NULL;
    this->pEntry = pEntry;

    //LOCAL VARIABLES
	bModbusError = false;
	ExpanderU6 = 0;
	ExpanderU7 = 0;
	WaterLevelAlerts = 0;
	bEmergencyStop = false;

    //OTHER VIRABLES
	eUserMode = Local;
	fTemperature = 0;

	//CREATE MENUS
	mMenuBar = new wxMenuBar();
	mMenuBar->SetFont(wxFont(9, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL));
	mMenuBar->SetBackgroundColour(wxColour(215, 215, 215));
	mOptionsMenu = new wxMenu();
	mHelpMenu = new wxMenu();
	mModeMenu = new wxMenu();

	//CONFIGURE DROPDOWN MENUS
	mHelpMenu->Append(wxID_INFO, _T("Informacje"));
	mOptionsMenu->Append(wxID_PREFERENCES, _T("Parametry Wykrywania Nieszczelności"));
	mModeMenu->Append(wxID_NETWORK, _T("Przejdź na Sterowanie Zdalne"));

	//CONFIGURE MENU BAR
	mMenuBar->Append(mModeMenu, _T("Sterowanie: Lokalne"));
	mMenuBar->Append(mOptionsMenu, _T("Opcje"));
	mMenuBar->Append(mHelpMenu, _T("Pomoc"));
	SetMenuBar(mMenuBar);

	//MAIN IMAGES
	imgMain[0] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/InnerOFFOuterOFF.png"), wxBITMAP_TYPE_PNG), wxPoint(20, -5), wxSize(848, 607));

	imgMain[1] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/InnerONOuterOFF.png"), wxBITMAP_TYPE_PNG), wxPoint(20, -5), wxSize(848, 607));
	imgMain[1]->Hide();

	imgMain[2] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/InnerOFFOuterON.png"), wxBITMAP_TYPE_PNG), wxPoint(20, -5), wxSize(848, 607));
	imgMain[2]->Hide();

	imgMain[3] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/InnerONOuterON.png"), wxBITMAP_TYPE_PNG), wxPoint(20, -5), wxSize(848, 607));
	imgMain[3]->Hide();

	//INNER PUMP IMAGES
	imgPumpInner[0] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpInnerOFF.png"), wxBITMAP_TYPE_PNG), wxPoint(141, 425), wxSize(53, 59));

	imgPumpInner[1] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpInnerOFF.png"), wxBITMAP_TYPE_PNG), wxPoint(141, 496), wxSize(53, 59));

	imgPumpInner[2] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpInnerOFF.png"), wxBITMAP_TYPE_PNG), wxPoint(141, 567), wxSize(53, 59));

	imgPumpInner[3] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpInnerON.png"), wxBITMAP_TYPE_PNG), wxPoint(141, 425), wxSize(53, 59));
	imgPumpInner[3]->Hide();

	imgPumpInner[4] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpInnerON.png"), wxBITMAP_TYPE_PNG), wxPoint(141, 496), wxSize(53, 59));
	imgPumpInner[4]->Hide();

	imgPumpInner[5] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpInnerON.png"), wxBITMAP_TYPE_PNG), wxPoint(141, 567), wxSize(53, 59));
	imgPumpInner[5]->Hide();

	imgPumpInner[6] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpInnerError.png"), wxBITMAP_TYPE_PNG), wxPoint(141, 425), wxSize(53, 59));
	imgPumpInner[6]->Hide();

	imgPumpInner[7] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpInnerError.png"), wxBITMAP_TYPE_PNG), wxPoint(141, 496), wxSize(53, 59));
	imgPumpInner[7]->Hide();

	imgPumpInner[8] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpInnerError.png"), wxBITMAP_TYPE_PNG), wxPoint(141, 567), wxSize(53, 59));
	imgPumpInner[8]->Hide();

	//OUTER PUMP IMAGES
	imgPumpOuter[0] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpOuterOFF.png"), wxBITMAP_TYPE_PNG), wxPoint(640, 427), wxSize(53, 59));

	imgPumpOuter[1] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpOuterOFF.png"), wxBITMAP_TYPE_PNG), wxPoint(640, 498), wxSize(53, 59));

	imgPumpOuter[2] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpOuterOFF.png"), wxBITMAP_TYPE_PNG), wxPoint(640, 569), wxSize(53, 59));

	imgPumpOuter[3] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpOuterON.png"), wxBITMAP_TYPE_PNG), wxPoint(640, 427), wxSize(53, 59));
	imgPumpOuter[3]->Hide();

	imgPumpOuter[4] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpOuterON.png"), wxBITMAP_TYPE_PNG), wxPoint(640, 498), wxSize(53, 59));
	imgPumpOuter[4]->Hide();

	imgPumpOuter[5] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpOuterON.png"), wxBITMAP_TYPE_PNG), wxPoint(640, 569), wxSize(53, 59));
	imgPumpOuter[5]->Hide();

	imgPumpOuter[6] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpOuterError.png"), wxBITMAP_TYPE_PNG), wxPoint(640, 427), wxSize(53, 59));
	imgPumpOuter[6]->Hide();

	imgPumpOuter[7] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpOuterError.png"), wxBITMAP_TYPE_PNG), wxPoint(640, 498), wxSize(53, 59));
	imgPumpOuter[7]->Hide();

	imgPumpOuter[8] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/PumpOuterError.png"), wxBITMAP_TYPE_PNG), wxPoint(640, 569), wxSize(53, 59));
	imgPumpOuter[8]->Hide();


	//VENT IMAGES
	imgVent[0] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/VentOFF.png"), wxBITMAP_TYPE_PNG), wxPoint(694, 90), wxSize(55, 55));

	imgVent[1] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/VentOFF.png"), wxBITMAP_TYPE_PNG), wxPoint(765, 90), wxSize(55, 55));

	imgVent[2] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/VentOFF.png"), wxBITMAP_TYPE_PNG), wxPoint(835, 90), wxSize(55, 55));

	imgVent[3] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/VentON.png"), wxBITMAP_TYPE_PNG), wxPoint(694, 90), wxSize(55, 55));
	imgVent[3]->Hide();

	imgVent[4] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/VentON.png"), wxBITMAP_TYPE_PNG), wxPoint(765, 90), wxSize(55, 55));
	imgVent[4]->Hide();

	imgVent[5] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/VentON.png"), wxBITMAP_TYPE_PNG), wxPoint(835, 90), wxSize(55, 55));
	imgVent[5]->Hide();

	imgVent[6] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/VentError.png"), wxBITMAP_TYPE_PNG), wxPoint(694, 90), wxSize(55, 55));
	imgVent[6]->Hide();

	imgVent[7] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/VentError.png"), wxBITMAP_TYPE_PNG), wxPoint(765, 90), wxSize(55, 55));
	imgVent[7]->Hide();

	imgVent[8] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/VentError.png"), wxBITMAP_TYPE_PNG), wxPoint(835, 90), wxSize(55, 55));
	imgVent[8]->Hide();

	//WATER LEVEL IMAGES
	imgWaterLevelRuler[0] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/WaterLevelInner.png"), wxBITMAP_TYPE_PNG), wxPoint(115, 264), wxSize(15, 70));

	imgWaterLevelRuler[1] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/WaterLevelOuter.png"), wxBITMAP_TYPE_PNG), wxPoint(723, 265), wxSize(15, 70));

	//WATER LEVEL I2C ERRORS
	imgWaterLevelErrorI2C[0] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/Exclamation.png"), wxBITMAP_TYPE_PNG), wxPoint(145, 259), wxSize(56, 56));
	imgWaterLevelErrorI2C[0]->Hide();
	imgWaterLevelErrorI2C[1] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/Exclamation.png"), wxBITMAP_TYPE_PNG), wxPoint(648, 259), wxSize(56, 56));
	imgWaterLevelErrorI2C[1]->Hide();

	//WATER LEVEL WARNINGS
	imgWaterLevelHigh = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/Exclamation.png"), wxBITMAP_TYPE_PNG), wxPoint(822, 355), wxSize(56, 56));
	imgWaterLevelHigh->Hide();
	txtWaterLevelHigh = new wxStaticText(this, wxID_ANY, _T("Wysoki\nPoziom\nWody w\nZbiorniku"), wxPoint(762, 419), wxSize(180, 70), wxALIGN_CENTRE_HORIZONTAL);
	txtWaterLevelHigh->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtWaterLevelHigh->SetForegroundColour(wxColour(207, 31, 37));
	txtWaterLevelHigh->Hide();

	imgWaterLevelLow = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/Exclamation.png"), wxBITMAP_TYPE_PNG), wxPoint(822, 355), wxSize(56, 56));
	imgWaterLevelLow->Hide();
	txtWaterLevelLow = new wxStaticText(this, wxID_ANY, _T("Niski\nPoziom\nWody w\nZbiorniku"), wxPoint(762, 419), wxSize(180, 70), wxALIGN_CENTRE_HORIZONTAL);
	txtWaterLevelLow->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtWaterLevelLow->SetForegroundColour(wxColour(207, 31, 37));
	txtWaterLevelLow->Hide();

	//SMALL WATER LEAK IMAGE AND TEXT
	imgWaterLeakSmall = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/WaterLeakRed.png"), wxBITMAP_TYPE_PNG), wxPoint(200, 122), wxSize(56, 56));
	imgWaterLeakSmall->Hide();
	txtWaterLeakSmall = new wxStaticText(this, wxID_ANY, _T("Podejrzenie\nNieszczelności"), wxPoint(140, 197), wxSize(180, 70), wxALIGN_CENTRE_HORIZONTAL);
	txtWaterLeakSmall->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtWaterLeakSmall->SetForegroundColour(wxColour(207, 31, 37));
	txtWaterLeakSmall->Hide();

	//LARGE WATER LEAK IMAGE AND TEXT
	imgWaterLeakLarge = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/WaterLeakRed.png"), wxBITMAP_TYPE_PNG), wxPoint(200, 122), wxSize(56, 56));
	imgWaterLeakLarge->Hide();
	txtWaterLeakLarge = new wxStaticText(this, wxID_ANY, _T("Wysokie Prawdopodobieństwo\nNieszczelności"), wxPoint(135, 197), wxSize(180, 70), wxALIGN_CENTRE_HORIZONTAL);
	txtWaterLeakLarge->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtWaterLeakLarge->SetForegroundColour(wxColour(207, 31, 37));
	txtWaterLeakLarge->Hide();

	//THERMOMETER IMAGES
	imgThermometer[0] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/Thermometer.png"), wxBITMAP_TYPE_PNG), wxPoint(486, 123), wxSize(13, 30));

	imgThermometer[1] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/Thermometer.png"), wxBITMAP_TYPE_PNG), wxPoint(525, 227), wxSize(13, 30));

	imgThermometer[2] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/Thermometer.png"), wxBITMAP_TYPE_PNG), wxPoint(525, 346), wxSize(13, 30));

	imgThermometer[3] = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/Thermometer.png"), wxBITMAP_TYPE_PNG), wxPoint(486, 452), wxSize(13, 30));

	//M2 MAGNET LOCK IMAGE AND TEXT
	imgMagnetLock = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/MagnetLock.png"), wxBITMAP_TYPE_PNG), wxPoint(373, 24), wxSize(60, 60));
	imgMagnetLock->Hide();

	txtMagnetLock = new wxStaticText(this, wxID_ANY, _T("Blokada Magnesu M2"), wxPoint(435, 45), wxSize(180, 50), wxALIGN_CENTRE_HORIZONTAL);
	txtMagnetLock->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtMagnetLock->SetForegroundColour(wxColour(207, 31, 37));
	txtMagnetLock->Hide();

	//MODBUS ERROR IMAGE AND TEXT
	imgModbusError = new wxStaticBitmap(this, wxID_ANY, wxBitmap(wxString::FromUTF8(getenv("HOME")) +
			wxString("/eclipse-workspace/Woda/Images/ModbusError.png"), wxBITMAP_TYPE_PNG), wxPoint(250, 252), wxSize(56, 56));
	imgModbusError->Hide();
	txtModbusError = new wxStaticText(this, wxID_ANY, _T("Błąd w Połączeniu\nz Kontrolerem"), wxPoint(190, 319), wxSize(180, 70), wxALIGN_CENTRE_HORIZONTAL);
	txtModbusError->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtModbusError->SetForegroundColour(wxColour(207, 31, 37));
	txtModbusError->Hide();

	//CYKLOTRON TITLE
	txtCyklotron = new wxStaticText(this, wxID_ANY, _T("Cyklotron"), wxPoint(168, 94), wxSize(120, 30), wxALIGN_CENTRE_HORIZONTAL);
	txtCyklotron->SetFont(wxFont(13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

	//WATER LEVEL TEXTS
	txtWaterLevelInner = new wxStaticText(this, wxID_ANY, _T("-"),  wxPoint(139, 289), wxSize(120, 30), wxALIGN_LEFT);
	txtWaterLevelInner->SetFont(wxFont(13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtWaterLevelOuter = new wxStaticText(this, wxID_ANY, _T("-"), wxPoint(193, 290), wxSize(120, 30), wxALIGN_RIGHT);
	txtWaterLevelOuter->SetFont(wxFont(13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

	//TEMPERATURE TEXTS
	posTemperatureDefault[0] = wxPoint(506, 127);
	posTemperatureDefault[1] = wxPoint(545, 232);
	posTemperatureDefault[2] = wxPoint(545, 351);
	posTemperatureDefault[3] = wxPoint(506, 457);
	txtTemperature[0] = new wxStaticText(this, wxID_ANY, "-", posTemperatureDefault[0], wxSize(120, 30), wxALIGN_LEFT);
	txtTemperature[0]->SetFont(wxFont(13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtTemperature[1] = new wxStaticText(this, wxID_ANY, "-", posTemperatureDefault[1], wxSize(120, 30), wxALIGN_LEFT);
	txtTemperature[1]->SetFont(wxFont(13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtTemperature[2] = new wxStaticText(this, wxID_ANY, "-", posTemperatureDefault[2], wxSize(120, 30), wxALIGN_LEFT);
	txtTemperature[2]->SetFont(wxFont(13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtTemperature[3] = new wxStaticText(this, wxID_ANY, "-", posTemperatureDefault[3], wxSize(120, 30), wxALIGN_LEFT);
	txtTemperature[3]->SetFont(wxFont(13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

	//EXPANDER I2C ERROR TEXTS
	txtErrorI2C[0] = new wxStaticText(this, wxID_ANY, _T("Błąd I2C"),  wxPoint(251, 457), wxSize(120, 30), wxALIGN_LEFT);
	txtErrorI2C[0]->SetFont(wxFont(12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtErrorI2C[0]->SetForegroundColour(wxColour(207, 31, 37));
	txtErrorI2C[1] = new wxStaticText(this, wxID_ANY, _T("Błąd I2C"),  wxPoint(759, 558), wxSize(120, 30), wxALIGN_LEFT);
	txtErrorI2C[1]->SetFont(wxFont(12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtErrorI2C[1]->SetForegroundColour(wxColour(207, 31, 37));
	txtErrorI2C[2] = new wxStaticText(this, wxID_ANY, _T("Błąd I2C"),  wxPoint(754, 18), wxSize(120, 30), wxALIGN_LEFT);
	txtErrorI2C[2]->SetFont(wxFont(12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
	txtErrorI2C[2]->SetForegroundColour(wxColour(207, 31, 37));

	//BUTTONS
	btnEmergencyStop = new wxButton(this, 10001, _T("Awaryjne Wyłączenie"), wxPoint(260, 535), wxSize(186, 38));
	btnEmergencyStop->SetBackgroundColour(wxColour(210, 210, 210));
	btnEmergencyStop->SetFont(wxFont(10, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

	//DISPLAY REFRESH TIMER
    tRefreshTimer = new wxTimer(this, 20001);
    tRefreshTimer->Start(DELAY_REFRESH_DISPLAY);

    //OTHER
    bLeakReset = false;
}

void cMain::RefreshDisplay(void)
{
	/******************************************* Copy Modbus Data ******************************************/

	if (mbMaster != NULL)
	{
		wxCriticalSectionLocker local_lock(mbMaster->local_guard);

		bModbusError = mbMaster->bModbusError;

		/******************************** Temperature Sensors **************************************/

		for (uint8_t i = 0; i < NUM_TEMP_SENSORS; i++)
		{
			Temperature[i] = mbMaster->Temperature[i];
			bTempSDError[i] = mbMaster->bTempSDError[i];
			bTempTimeoutError[i] = mbMaster->bTempTimeoutError[i];
		}

		/************************************ I2C Errors *******************************************/

		for (uint8_t i = 0; i < NUM_DEVICES_I2C; i++)
		{
			bI2CError[i] = mbMaster->bI2CError[i];
		}

		/*********************************** Water Levels ******************************************/

		for (uint8_t i = 0; i < NUM_WATER_READINGS; i++)
		{
			WaterLevelInner[i] = mbMaster->WaterLevelInner[i];
			WaterLevelOuter[i] = mbMaster->WaterLevelOuter[i];
		}

		/****************************** Emergency Stop Status **************************************/

		bEmergencyStop = mbMaster->bEmergencyStop;

		/************************* Expanders and Water Level Alerts ********************************/

		WaterLevelAlerts = mbMaster->WaterLevelAlerts;
		ExpanderU6 = mbMaster->ExpanderU6;
		ExpanderU7 = mbMaster->ExpanderU7;

		/***************************** Water Leak Parameters ***************************************/

		fLeakDetect[0] = mbMaster->fLeakDetect[0];
		fLeakDetect[1] = mbMaster->fLeakDetect[1];

		/************************************ Coils ************************************************/

		for (uint8_t i = 0; i < MB_NUM_COILS; i++) mbCoils[i] = mbMaster->mbCoils[i];
	}

	/********************************************* UI Elements  ********************************************/

	if (bModbusError) //CONNECTION ERROR
	{
		if (!imgModbusError->IsShown()) imgModbusError->Show();
		if (!txtModbusError->IsShown()) txtModbusError->Show();

		//Main images and pumps
		for (uint8_t i = 0; i < NUM_MAIN_IMAGES; i++)
		{
			if (i == 0)
			{
				if (!imgMain[i]->IsShown()) imgMain[i]->Show();
			}
			else if (imgMain[i]->IsShown()) imgMain[i]->Hide();
		}

		//Pump images
		for (uint8_t i = 0; i < NUM_PUMP_IMAGES; i++)
		{
			if (i >= 0 && i <= 2)
			{
				if (!imgPumpInner[i]->IsShown()) imgPumpInner[i]->Show();
				if (!imgPumpOuter[i]->IsShown()) imgPumpOuter[i]->Show();
			}
			else
			{
				if (imgPumpInner[i]->IsShown()) imgPumpInner[i]->Hide();
				if (imgPumpOuter[i]->IsShown()) imgPumpOuter[i]->Hide();
			}
		}

		//Vent images
		for (uint8_t i = 0; i < NUM_VENT_IMAGES; i++)
		{
			if (i >= 0 && i <= 2)
			{
				if (!imgVent[i]->IsShown()) imgVent[i]->Show();
			}
			else if (imgVent[i]->IsShown()) imgVent[i]->Hide();
		}

		//Water leak alerts
		if (imgWaterLeakSmall->IsShown()) imgWaterLeakSmall->Hide();
		if (txtWaterLeakSmall->IsShown()) txtWaterLeakSmall->Hide();
		if (imgWaterLeakLarge->IsShown()) imgWaterLeakLarge->Hide();
		if (txtWaterLeakLarge->IsShown()) txtWaterLeakLarge->Hide();

		//Water level alerts
		if (imgWaterLevelLow->IsShown()) imgWaterLevelLow->Hide();
		if (imgWaterLevelHigh->IsShown()) imgWaterLevelHigh->Hide();
		if (txtWaterLevelLow->IsShown()) txtWaterLevelLow->Hide();
		if (txtWaterLevelHigh->IsShown()) txtWaterLevelHigh->Hide();

		//M2 maget lock
		if (imgMagnetLock->IsShown()) imgMagnetLock->Hide();
		if (txtMagnetLock->IsShown()) txtMagnetLock->Hide();

		//Expander I2C Errors
		for (uint8_t i = 0; i < 3; i++) if (txtErrorI2C[i]->IsShown()) txtErrorI2C[i]->Hide();

		//Emergency stop button
		if (btnEmergencyStop->IsEnabled()) btnEmergencyStop->Disable();

		//Windows
		if (pUserMessage) pUserMessage->Close();
		if (pLeakParameters) pLeakParameters->Close();
		if (pLeakAlert) pLeakAlert->Destroy();

		//Menu items
		if (mOptionsMenu->IsEnabled(wxID_PREFERENCES)) mOptionsMenu->Enable(wxID_PREFERENCES, FALSE);

		//Inner water level readings and errors
		if (imgWaterLevelErrorI2C[0]->IsShown()) imgWaterLevelErrorI2C[0]->Hide();
		txtWaterLevelInner->SetLabel(_T("-"));
		txtWaterLevelInner->SetPosition(wxPoint(139, 288));
		txtWaterLevelInner->SetFont(wxFont(13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
		txtWaterLevelInner->SetForegroundColour(wxColour());

		//Outer water level readings and errors
		if (imgWaterLevelErrorI2C[1]->IsShown()) imgWaterLevelErrorI2C[1]->Hide();
		txtWaterLevelOuter->SetLabel(_T("-"));
		txtWaterLevelOuter->SetPosition(wxPoint(593, 290));
		txtWaterLevelOuter->SetFont(wxFont(13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
		txtWaterLevelOuter->SetForegroundColour(wxColour());

		//Temperature readings
		for (uint8_t i = 0; i < NUM_TEMP_SENSORS; i++)
		{
			txtTemperature[i]->SetLabel(_T("-"));
			txtTemperature[i]->SetForegroundColour(wxColour());
			txtTemperature[i]->SetPosition(posTemperatureDefault[i] - wxPoint(0, 1));
			txtTemperature[i]->SetFont(wxFont(13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
		}
	}
	else //NORMAL OPERATION
	{
		/******************************** Modbus Errors ********************************************/

		if (imgModbusError->IsShown()) imgModbusError->Hide();
		if (txtModbusError->IsShown()) txtModbusError->Hide();

		/*************************** Modes, Buttons and Menus **************************************/

		if (eUserMode == Local)
		{
			//Emergency stop button
			if (!pUserMessage)
			{
				if (!bI2CError[6] && (ExpanderU6 & 0x06) != 0) //check if pumps are ON
				{
					if (!btnEmergencyStop->IsEnabled()) btnEmergencyStop->Enable();
				}
				else if (btnEmergencyStop->IsEnabled()) btnEmergencyStop->Disable();
			}
			else if (btnEmergencyStop->IsEnabled()) btnEmergencyStop->Disable();

			//User menus
			if (!mOptionsMenu->IsEnabled(wxID_PREFERENCES)) mOptionsMenu->Enable(wxID_PREFERENCES, TRUE);
		}
		else if (eUserMode == Remote)
		{
			if (btnEmergencyStop->IsEnabled()) btnEmergencyStop->Disable();
			if (mOptionsMenu->IsEnabled(wxID_PREFERENCES)) mOptionsMenu->Enable(wxID_PREFERENCES, FALSE);
		}


		/********************************** Main Images ********************************************/

		if (pUserMessage)
		{
			if (pUserMessage->tEmergencyStopTimer->IsRunning() && ((ExpanderU6 & 0x07) == 0))
			{
				mbMaster->WaterEmergencyStop(false);
				pUserMessage->Close();
			}
		}

		/********************************** Main Images ********************************************/

		if (!bI2CError[6])
		{
			//Hide U6 Expander errors
			if (txtErrorI2C[0]->IsShown()) txtErrorI2C[0]->Hide();
			if (txtErrorI2C[1]->IsShown()) txtErrorI2C[1]->Hide();

			if ((ExpanderU6 & 0x3F) == 0) //all pumps OFF
			{
				if (imgMain[1]->IsShown()) imgMain[1]->Hide();
				if (imgMain[2]->IsShown()) imgMain[2]->Hide();
				if (imgMain[3]->IsShown()) imgMain[3]->Hide();
				if (!imgMain[0]->IsShown()) imgMain[0]->Show();
			}
			else if ((ExpanderU6 & 0x3F) <= 0x07) //inner pumps ON, outer pumps OFF
			{
				if (imgMain[0]->IsShown()) imgMain[0]->Hide();
				if (imgMain[2]->IsShown()) imgMain[2]->Hide();
				if (imgMain[3]->IsShown()) imgMain[3]->Hide();
				if (!imgMain[1]->IsShown()) imgMain[1]->Show();
			}
			else if ((ExpanderU6 & 0x3F) >= 0x08)
			{
				if ((ExpanderU6 & 0x07) == 0) //inner pumps OFF, outer pumps ON
				{
					if (imgMain[0]->IsShown()) imgMain[0]->Hide();
					if (imgMain[1]->IsShown()) imgMain[1]->Hide();
					if (imgMain[3]->IsShown()) imgMain[3]->Hide();
					if (!imgMain[2]->IsShown()) imgMain[2]->Show();
				}
				else //inner and outer pumps ON
				{
					if (imgMain[0]->IsShown()) imgMain[0]->Hide();
					if (imgMain[1]->IsShown()) imgMain[1]->Hide();
					if (imgMain[2]->IsShown()) imgMain[2]->Hide();
					if (!imgMain[3]->IsShown()) imgMain[3]->Show();
				}
			}
		}
		else //U6 Expander I2C error
		{
			if (imgMain[1]->IsShown()) imgMain[1]->Hide();
			if (imgMain[2]->IsShown()) imgMain[2]->Hide();
			if (imgMain[3]->IsShown()) imgMain[3]->Hide();
			if (!imgMain[0]->IsShown()) imgMain[0]->Show();
			if (!txtErrorI2C[0]->IsShown()) txtErrorI2C[0]->Show();
			if (!txtErrorI2C[1]->IsShown()) txtErrorI2C[1]->Show();
		}

		/********************************** Inner Pumps *********************************************/

		for (uint8_t i = 0; i < (NUM_PUMPS / 2); i++)
		{
			//Check for I2C errors
			if (!bI2CError[6])
			{
				//Normal operation
				if ((ExpanderU6 & (1 << i)) == 0) //pump OFF
				{
					//Hide errors and pump OFF images
					if (imgPumpInner[i + 3]->IsShown()) imgPumpInner[i + 3]->Hide();
					if (imgPumpInner[i + 6]->IsShown()) imgPumpInner[i + 6]->Hide();

					//Show pump ON image
					if (!imgPumpInner[i]->IsShown()) imgPumpInner[i]->Show();
				}
				else //pump ON
				{
					//Hide errors and pump OFF images
					if (imgPumpInner[i]->IsShown()) imgPumpInner[i]->Hide();
					if (imgPumpInner[i + 6]->IsShown()) imgPumpInner[i + 6]->Hide();

					//Show pump ON image
					if (!imgPumpInner[i + 3]->IsShown()) imgPumpInner[i + 3]->Show();
				}
			}
			else //U6 Expander I2C errors
			{
				//Hide pump OFF and pump ON images
				if (imgPumpInner[i]->IsShown()) imgPumpInner[i]->Hide();
				if (imgPumpInner[i + 3]->IsShown()) imgPumpInner[i + 3]->Hide();

				//Show I2C error image
				if (!imgPumpInner[i + 6]->IsShown()) imgPumpInner[i + 6]->Show();
			}
		}

		/********************************** Outer Pumps *********************************************/

		for (uint8_t i = 0; i < (NUM_PUMPS / 2); i++)
		{
			//Check for I2C errors
			if (!bI2CError[6])
			{
				//Normal operation
				if ((ExpanderU6 & (1 << (i + 3))) == 0) //pump OFF
				{
					//Hide errors and pump OFF images
					if (imgPumpOuter[i + 3]->IsShown()) imgPumpOuter[i + 3]->Hide();
					if (imgPumpOuter[i + 6]->IsShown()) imgPumpOuter[i + 6]->Hide();

					//Show pump ON image
					if (!	imgPumpOuter[i]->IsShown()) imgPumpOuter[i]->Show();
				}
				else //pump ON
				{
					//Hide errors and pump OFF images
					if (imgPumpOuter[i]->IsShown()) imgPumpOuter[i]->Hide();
					if (imgPumpOuter[i + 6]->IsShown()) imgPumpOuter[i + 6]->Hide();

					//Show pump ON image
					if (!imgPumpOuter[i + 3]->IsShown()) imgPumpOuter[i + 3]->Show();
				}
			}
			else //U6 Expander I2C errors
			{
				//Hide pump OFF and pump ON images
				if (imgPumpOuter[i]->IsShown()) imgPumpOuter[i]->Hide();
				if (imgPumpOuter[i + 3]->IsShown()) imgPumpOuter[i + 3]->Hide();

				//Show I2C error image
				if (!imgPumpOuter[i + 6]->IsShown()) imgPumpOuter[i + 6]->Show();
			}
		}

		/************************************* Vents ************************************************/

		for (uint8_t i = 0; i < NUM_VENTS; i++)
		{
			//Check for I2C errors
			if (!bI2CError[7])
			{
				//Hide U7 Expander I2C error
				if (txtErrorI2C[2]->IsShown()) txtErrorI2C[2]->Hide();

				//Normal operation
				if ((ExpanderU7 & (1 << i)) == 0) //vent OFF
				{
					//Hide errors and vent OFF images
					if (imgVent[i + 3]->IsShown()) imgVent[i + 3]->Hide();
					if (imgVent[i + 6]->IsShown()) imgVent[i + 6]->Hide();

					//Show vent ON image
					if (!imgVent[i]->IsShown()) imgVent[i]->Show();
				}
				else //vent ON
				{
					//Hide errors and vent OFF images
					if (imgVent[i]->IsShown()) imgVent[i]->Hide();
					if (imgVent[i + 6]->IsShown()) imgVent[i + 6]->Hide();

					//Show vent ON imageRefreshDisplay();
					if (!imgVent[i + 3]->IsShown()) imgVent[i + 3]->Show();
				}
			}
			else //U7 Expander I2C error
			{
				//Hide vent OFF and vent ON images
				if (imgVent[i]->IsShown()) imgVent[i]->Hide();
				if (imgVent[i + 3]->IsShown()) imgVent[i + 3]->Hide();

				//Show I2C error image
				if (!imgVent[i + 6]->IsShown()) imgVent[i + 6]->Show();

				//Show I2C error text
				if (!txtErrorI2C[2]->IsShown()) txtErrorI2C[2]->Show();
			}
		}

		/******************************* Water Level Alerts *****************************************/

		if ((WaterLevelAlerts & 0x01) != 0) //water level low
		{
			if (imgWaterLevelHigh->IsShown()) imgWaterLevelHigh->Hide();
			if (txtWaterLevelHigh->IsShown()) txtWaterLevelHigh->Hide();
			if (!imgWaterLevelLow->IsShown()) imgWaterLevelLow->Show();
			if (!txtWaterLevelLow->IsShown()) txtWaterLevelLow->Show();
		}
		else if ((WaterLevelAlerts & 0x02) != 0) //water level high
		{
			if (imgWaterLevelLow->IsShown()) imgWaterLevelLow->Hide();
			if (txtWaterLevelLow->IsShown()) txtWaterLevelLow->Hide();
			if (!imgWaterLevelHigh->IsShown()) imgWaterLevelHigh->Show();
			if (!txtWaterLevelHigh->IsShown()) txtWaterLevelHigh->Show();
		}
		else //hide icons
		{
			if (imgWaterLevelLow->IsShown()) imgWaterLevelLow->Hide();
			if (imgWaterLevelHigh->IsShown()) imgWaterLevelHigh->Hide();
			if (txtWaterLevelLow->IsShown()) txtWaterLevelLow->Hide();
			if (txtWaterLevelHigh->IsShown()) txtWaterLevelHigh->Hide();
		}

		/****************************** Water Leak Detection ****************************************/

		if ((WaterLevelAlerts & 0x08) != 0) //large leak
		{
			//Hide small leak image and text
			if (imgWaterLeakSmall->IsShown()) imgWaterLeakSmall->Hide();
			if (txtWaterLeakSmall->IsShown()) txtWaterLeakSmall->Hide();

			//Show large leak image and text
			if (!imgWaterLeakLarge->IsShown()) imgWaterLeakLarge->Show();
			if (!txtWaterLeakLarge->IsShown()) txtWaterLeakLarge->Show();
		}
		else if ((WaterLevelAlerts & 0x04) != 0) //small leak
		{
			//Hide large leak image and text
			if (imgWaterLeakLarge->IsShown()) imgWaterLeakLarge->Hide();
			if (txtWaterLeakLarge->IsShown()) txtWaterLeakLarge->Hide();

			//Show small leak image and text
			if (!imgWaterLeakSmall->IsShown()) imgWaterLeakSmall->Show();
			if (!txtWaterLeakSmall->IsShown()) txtWaterLeakSmall->Show();
		}
		else //hide all icons and text
		{
			//Hide small leak image and text
			if (imgWaterLeakSmall->IsShown()) imgWaterLeakSmall->Hide();
			if (txtWaterLeakSmall->IsShown()) txtWaterLeakSmall->Hide();

			//Hide large leak image and text
			if (imgWaterLeakLarge->IsShown()) imgWaterLeakLarge->Hide();
			if (txtWaterLeakLarge->IsShown()) txtWaterLeakLarge->Hide();
		}

		//Create alert window
		if ((WaterLevelAlerts & 0x0C) != 0 && !pLeakAlert)
		{
			bLeakReset = false;
			pLeakAlert = new cLeakAlert(this);
			pLeakAlert->SetPosition(this->GetPosition() + (this->GetSize() / 2) - (pLeakAlert->GetSize() / 2));
			pLeakAlert->SetIcon(this->GetIcon());
			pLeakAlert->Show();
		}
		else if ((WaterLevelAlerts & 0x0C) == 0 && pLeakAlert && bLeakReset)
		{
			pLeakAlert->Destroy();
			pLeakAlert = NULL;
		}

		/****************************** Water Level Readings ****************************************/

		//Inner Tank
		if (!bI2CError[4])
		{
			//Inner tank
			WaterLevelInner[0] = (uint16_t) ((float) WaterLevelInner[0] / (MAX_LEVEL_ADC / MAX_LEVEL));
			if (imgWaterLevelErrorI2C[0]->IsShown()) imgWaterLevelErrorI2C[0]->Hide();
			txtWaterLevelInner->SetPosition(wxPoint(139, 289));
			txtWaterLevelInner->SetFont(wxFont(13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
			txtWaterLevelInner->SetLabel(wxString::Format(wxT("%i"), WaterLevelInner[0]) + _T("mm")); //use 1s average
			txtWaterLevelInner->SetForegroundColour(wxColour());
		}
		else
		{
			//Display I2C errors
			txtWaterLevelInner->SetPosition(wxPoint(140, 321));
			txtWaterLevelInner->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
			txtWaterLevelInner->SetLabel(_T("Błąd I2C"));
			txtWaterLevelInner->SetForegroundColour(wxColour(207, 31, 37));
			if (!imgWaterLevelErrorI2C[0]->IsShown()) imgWaterLevelErrorI2C[0]->Show();
		}

		//Outer tank
		if (!bI2CError[5])
		{
			//Outer tank
			WaterLevelOuter[0] = WaterLevelOuter[0] / (MAX_LEVEL_ADC / MAX_LEVEL);
			if (imgWaterLevelErrorI2C[1]->IsShown()) imgWaterLevelErrorI2C[1]->Hide();
			txtWaterLevelOuter->SetPosition(wxPoint(593, 290));
			txtWaterLevelOuter->SetFont(wxFont(13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
			txtWaterLevelOuter->SetLabel(wxString::Format(wxT("%i"), WaterLevelOuter[0]) + _T("mm")); //use 1s average
			txtWaterLevelOuter->SetForegroundColour(wxColour());
		}
		else
		{
			//Display I2C errors
			txtWaterLevelOuter->SetPosition(wxPoint(592, 321));
			txtWaterLevelOuter->SetFont(wxFont(11, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
			txtWaterLevelOuter->SetLabel(_T("Błąd I2C"));
			txtWaterLevelOuter->SetForegroundColour(wxColour(207, 31, 37));
			if (!imgWaterLevelErrorI2C[1]->IsShown()) imgWaterLevelErrorI2C[1]->Show();
		}


		/********************************* M2 Magnet Lock *******************************************/

		if (!bI2CError[7])
		{
			if ((ExpanderU7 & 0x08) != 0)
			{
				if (!imgMagnetLock->IsShown()) imgMagnetLock->Show();
				if (!txtMagnetLock->IsShown()) txtMagnetLock->Show();
			}
			else
			{
				if (imgMagnetLock->IsShown()) imgMagnetLock->Hide();
				if (txtMagnetLock->IsShown()) txtMagnetLock->Hide();
			}
		}
		else
		{
			if (imgMagnetLock->IsShown()) imgMagnetLock->Hide();
			if (txtMagnetLock->IsShown()) txtMagnetLock->Hide();
		}

		/****************************** Temperature Readings ****************************************/

		//Convert and display DS18B20 sensor readings
		for (uint8_t i = 0; i < NUM_TEMP_SENSORS; i++)
		{
			if (bI2CError[i]) //I2C errors
			{
				//Display I2C error
				txtTemperature[i]->SetFont(wxFont(12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
				txtTemperature[i]->SetLabel(_T("Błąd I2C"));
				txtTemperature[i]->SetForegroundColour(wxColour(207, 31, 37));
				txtTemperature[i]->SetPosition(posTemperatureDefault[i] + wxPoint(0, 1));
			}
			else if (bTempTimeoutError[i]) //timeout error
			{
				txtTemperature[i]->SetFont(wxFont(12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
				txtTemperature[i]->SetLabel(_T("Błąd TO"));
				txtTemperature[i]->SetForegroundColour(wxColour(207, 31, 37));
				txtTemperature[i]->SetPosition(posTemperatureDefault[i] + wxPoint(0, 1));
			}
			else if (bTempSDError[i])
			{
				txtTemperature[i]->SetFont(wxFont(12, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
				txtTemperature[i]->SetLabel(_T("Błąd SD"));
				txtTemperature[i]->SetForegroundColour(wxColour(207, 31, 37));
				txtTemperature[i]->SetPosition(posTemperatureDefault[i] + wxPoint(0, 1));
			}
			else //normal operation
			{
				if (Temperature[i] & 0x8000) //negative temperature
				{
					//Get 2's complement
					Temperature[i] = (Temperature[i] ^ 0xFFFF) + 1;

					//Convert to floating point
					fTemperature = (-1) * ((Temperature[i] >> 4)
										+ (((Temperature[i] & 0x8) >> 3) * 0.5f)
										+ (((Temperature[i] & 0x4) >> 2) * 0.25f)
										+ (((Temperature[i] & 0x2) >> 1) * 0.125f));
				}
				else //positive temperature
				{
					fTemperature = ((Temperature[i] >> 4)
								 + (((Temperature[i] & 0x8) >> 3) * 0.5f)
								 + (((Temperature[i] & 0x4) >> 2) * 0.25f)
								 + (((Temperature[i] & 0x2) >> 1) * 0.125f));
				}

				//Display reading
				txtTemperature[i]->SetFont(wxFont(13, wxFONTFAMILY_DEFAULT, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));
				txtTemperature[i]->SetLabel(wxString::Format(wxT("%.1f"), fTemperature) + _T("\x2103"));
				txtTemperature[i]->SetForegroundColour(wxColour());
				txtTemperature[i]->SetPosition(posTemperatureDefault[i]);
			}
		}
	}

	//Refresh and update frame
	this->Refresh();
	this->Update();
}

void cMain::OnRefreshDisplay(wxTimerEvent& evt)
{
	RefreshDisplay();
}

/* Menu clicked event */
void cMain::OnMenuClickedInformation(wxCommandEvent& evt)
{
    if (pInformation) //window already open
    {
        pInformation->Raise(); //show window
    }
    else
    {
        pInformation = new cInformation(this);
        pInformation->SetPosition(this->GetPosition());
        pInformation->SetIcon(this->GetIcon());
        pInformation->Show();
    }

    evt.Skip();
}

/* Menu clicked event */
void cMain::OnMenuClickedParameters(wxCommandEvent& evt)
{
    if (pLeakParameters) //window already open
    {
        pLeakParameters->Raise(); //show window
    }
    else
    {
        pLeakParameters = new cLeakParameters(this);
        pLeakParameters->SetPosition(this->GetPosition());
        pLeakParameters->SetIcon(this->GetIcon());
        pLeakParameters->Show();
    }

    evt.Skip();
}

/* Menu clicked event */
void cMain::OnMenuClickedMode(wxCommandEvent& evt)
{
	/************************************ Update Frame Title ***********************************/

	if (eUserMode == Local)
	{
	    this->SetTitle(_T("Woda (Sterowanie Zdalne)"));
		mMenuBar->SetMenuLabel(0, _T("Sterowanie: Zdalne"));
		mModeMenu->SetLabel(wxID_NETWORK, _T("Przejdź na Sterowanie Lokalne"));
		eUserMode = Remote;
	}
	else if (eUserMode == Remote)
	{
	    this->SetTitle(_T("Woda"));
		mMenuBar->SetMenuLabel(0, _T("Sterowanie: Lokalne"));
		mModeMenu->SetLabel(wxID_NETWORK, _T("Przejdź na Sterowanie Zdalne"));
		eUserMode = Local;
	}

	/************************************ Remote Data ******************************************/

	if (mbMaster != NULL)
	{
		wxCriticalSectionLocker remote_lock(mbMaster->remote_guard);
		mbMaster->mb_mapping->tab_input_registers[MB_INPUTREG_USERMODE - 1] = (uint16_t) (eUserMode == Local);
	}

    evt.Skip();
}

/* Frame closed */
void cMain::OnClose(wxCloseEvent&)
{
	TerminateApp(false);
}

void cMain::TerminateApp(bool bInitError)
{
	/******************************************* Stop Refresh **********************************************/

	if (tRefreshTimer->IsRunning()) tRefreshTimer->Stop();

	/******************************************* Close Other Windows ***************************************/

	if (pInformation) pInformation->Close();
	if (pLeakParameters) pLeakParameters->Close();
	if (pUserMessage) pUserMessage->Close();
	if (pLeakAlert) pLeakAlert->Destroy();

	/******************************************* Clear Modbus Pointer **************************************/

	if (mbMaster != NULL) mbMaster->pMain = NULL;

	/******************************************* Destroy Frame *********************************************/

	Destroy();
}
