#pragma once

#include "wParameters.h"
#include "cModbusMaster.h"

class cThreadTCP;
class cModbusMaster;

/* Modbus TCP/IP thread */
class cThreadTCP : public wxThread
{
	public:
		cThreadTCP(cModbusMaster*);
		~cThreadTCP();

	protected:
		virtual ExitCode Entry();

	private:
		void RestartSocketTCP(modbus_t*);

	private: //TCP/IP configuration
		cModbusMaster* mbMaster;
		uint8_t *tcpQuery;
		modbus_t* ctxTCP;
		int32_t socket;
		int32_t rc;
		int32_t rp;
		int32_t ct;

	private:
		int32_t regNum;
		int32_t regStart;
		uint16_t regHoldingTemp;
		float fNewSettings[MB_NUM_HOLDING_REGISTERS] = { 0 };
		char cNewSetting[6] = { 0 };
};
