#pragma once

//EXTERNAL LIBRARIES
#include <boost/algorithm/string.hpp>
#include <regex.h>
#include <string>
#include <locale>
#include <vector>
#include <modbus.h>
#include <unistd.h>
#include <algorithm>
#include <chrono>
#include <ifaddrs.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <limits.h>
#include <map>
#include <wx/wx.h>
#include <wx/log.h>
#include <wx/textfile.h>
#include <wx/snglinst.h>
#include <wx/progdlg.h>
#include <wx/statline.h>
#include <wx/sound.h>

//NAMESPACES
using namespace std;

//APPLICATION MODES
typedef enum { Local = 0, Remote = 1} eMode;

//THREAD DELAYS
const int32_t DELAY_MODBUS_RTU = 257; //ms, Modbus RTU thread delay
const int32_t DELAY_REFRESH_DISPLAY = 211;  //ms, main display refreshconst int32_t DELAY_REFRESH_DISPLAY = 211;  //ms, main display refresh
const int32_t DELAY_ALERT_SOUND = 2000; //ms delay between alert sounds
const int32_t DELAY_CONNECTION_TCP	= 100;  //ms, delay between subsequent TCP connection attempts
const int32_t DELAY_CLOSE_APP = 1000; //ms, delay before closing app

//TIMEOUTS
const int32_t CLOSE_APP_TIMEOUT = 2000; //ms
const int32_t EMERGENCY_STOP_TIMEOUT = 5000; //ms

//MODBUS CONFIGURATION
const int32_t MB_BAUD = 19200; //Modbus RTU Baud Rate
const int32_t MB_RESPONSE_TIMEOUT_SEC = 0; //Modbus RTU response timeout (seconds)
const int32_t MB_RESPONSE_TIMEOUT_US = 75000; //Modbus RTU response timeout (us)
const int32_t MB_INDICATION_TIMEOUT_US = 150000; //Modbus TCP/IP Indication timeout (us)

//OTHER
const int32_t NUM_PUMPS = 6;
const int32_t NUM_VENTS = 3;
const int32_t NUM_TEMP_SENSORS   = 4;
const int32_t NUM_WATER_READINGS = 4;
const int32_t NUM_DEVICES_I2C    = 10;

//IMAGES
const uint8_t NUM_MAIN_IMAGES = 4;
const uint8_t NUM_PUMP_IMAGES = 9;
const uint8_t NUM_VENT_IMAGES = 9;

//WATER LEVEL CONVERSION
const float MAX_LEVEL = 2000;
const float MAX_LEVEL_ADC = 0x7FFF;

//LEAK DETECTION
const int32_t NUM_LEAK_DETECT = 2;
const float MIN_LEAK = 0.0f;
const float MAX_LEAK = 100.0f;

//MODBUS RTU THREADS
const uint8_t MB_THREAD_WRITE_COIL    = 0;
const uint8_t MB_THREAD_WRITE_HOLDING = 1;

//MODBUS REGISTERS
const int32_t MB_NUM_COILS			   = 2;
const int32_t MB_NUM_DISCRETE_INPUTS   = 0;
const int32_t MB_NUM_INPUT_REGISTERS   = 20;
const int32_t MB_NUM_HOLDING_REGISTERS = 2;

//MODBUS TCP/IP INPUT REGISTER MAPPING
const uint8_t MB_INPUTREG_TEMP_START     = 1;
const uint8_t MB_INPUTREG_TEMP_END       = 4;
const uint8_t MB_INPUTREG_TEMP_SD 	     = 5;
const uint8_t MB_INPUTREG_TEMP_TIMEOUT   = 6;
const uint8_t MB_INPUTREG_I2C_ERRORS     = 7;
const uint8_t MB_INPUTREG_EXPANDER_U6    = 8;
const uint8_t MB_INPUTREG_EXPANDER_U7    = 9;
const uint8_t MB_INPUTREG_WATERIN_START  = 10;
const uint8_t MB_INPUTREG_WATERIN_END    = 14;
const uint8_t MB_INPUTREG_WATEROUT_START = 15;
const uint8_t MB_INPUTREG_WATEROUT_END   = 19;
const uint8_t MB_INPUTREG_WATER_ALERTS   = 20;
const uint8_t MB_INPUTREG_RTU_ERROR      = 21;
const uint8_t MB_INPUTREG_USERMODE 		 = 22;

//MODBUS TCP/IP HOLDING REGISTER MAPPING
const uint8_t MB_HOLDING_LEAK_SMALL = 1;
const uint8_t MB_HOLDING_LEAK_LARGE = 2;

//MODBUS TCP/IP EVENT HANDLER PARAMETERS
const int32_t MAX_REMOTE_EVENTS = 100;
