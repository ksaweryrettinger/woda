#pragma once

#include "wParameters.h"
#include "cLeakParameters.h"
#include "cUserMessage.h"
#include "cLeakAlert.h"
#include "cInformation.h"
#include "cModbusMaster.h"

class cEntry;
class cModbusMaster;
class cLeakParameters;
class cUserMessage;
class cLeakAlert;
class cInformation;

class cMain : public wxFrame
{
	public: //constructor
		cMain(cEntry*);
		void TerminateApp(bool);

	private: //event handlers
		void OnMenuClickedInformation(wxCommandEvent& evt);
		void OnMenuClickedParameters(wxCommandEvent& evt);
		void OnMenuClickedMode(wxCommandEvent& evt);
		void OnRefreshDisplay(wxTimerEvent& evt);
		void OnButtonEmergencyStop(wxCommandEvent& evt);
		void OnClose(wxCloseEvent& evt);

	private: //other methods
		void RefreshDisplay(void);

	public: //class pointers
		cEntry* pEntry;
		cModbusMaster* mbMaster;
		cLeakParameters* pLeakParameters;
		cUserMessage* pUserMessage;
		cLeakAlert* pLeakAlert;
		cInformation* pInformation;
		wxMenu* mOptionsMenu;
		wxMenu* mHelpMenu;

	public: //other public variables
		wxCriticalSection main_guard; //main data guard
		eMode eUserMode;

	private: //menu
		wxMenuBar* mMenuBar;
		wxMenu* mModeMenu;

	private: //UI elements

		//IMAGES
		wxStaticBitmap* imgMain[NUM_MAIN_IMAGES];
		wxStaticBitmap* imgPumpInner[NUM_PUMP_IMAGES];
		wxStaticBitmap* imgPumpOuter[NUM_PUMP_IMAGES];
		wxStaticBitmap* imgVent[NUM_VENT_IMAGES];
		wxStaticBitmap* imgWaterLevelRuler[2];
		wxStaticBitmap* imgWaterLevelErrorI2C[2];
		wxStaticBitmap* imgThermometer[4];
		wxStaticBitmap* imgWaterLeakSmall;
		wxStaticBitmap* imgWaterLeakLarge;
		wxStaticBitmap* imgWaterLevelHigh;
		wxStaticBitmap* imgWaterLevelLow;
		wxStaticBitmap* imgMagnetLock;
		wxStaticBitmap* imgModbusError;

		//STATIC TEXTS
		wxStaticText* txtModbusError;
		wxStaticText* txtMagnetLock;
		wxStaticText* txtCyklotron;
		wxStaticText* txtWaterLevelInner;
		wxStaticText* txtWaterLevelOuter;
		wxStaticText* txtWaterLeakSmall;
		wxStaticText* txtWaterLeakLarge;
		wxStaticText* txtTemperature[NUM_TEMP_SENSORS];
		wxStaticText* txtWaterLevelLow;
		wxStaticText* txtWaterLevelHigh;
		wxStaticText* txtErrorI2C[3];

		//POSITIONS
		wxPoint posTemperatureDefault[NUM_TEMP_SENSORS];

		//BUTTONS
		wxButton* btnEmergencyStop;

	private: //Other

		float fTemperature;

	private: //Shared Data

		//Leak detection
		float fLeakDetect[2] = { 0 };

		//Emergency water stop
		bool bEmergencyStop;

		//Errors
		bool bModbusError;
		bool bTempSDError[NUM_TEMP_SENSORS] = { false };
		bool bTempTimeoutError[NUM_TEMP_SENSORS] = { false };
		bool bI2CError[NUM_DEVICES_I2C] = { false };

		//Readings
		uint8_t ExpanderU6;
		uint8_t ExpanderU7;
		uint16_t Temperature[NUM_TEMP_SENSORS] = { 0 };
		uint16_t WaterLevelInner[NUM_WATER_READINGS] = { 0 };
		uint16_t WaterLevelOuter[NUM_WATER_READINGS] = { 0 };
		uint16_t WaterLevelAlerts;

		//Coils
		uint8_t mbCoils[2] = { 0 };

	private: //other data
		wxTimer* tRefreshTimer;
		bool bLeakReset;

		friend class cLeakParameters;
		friend class cUserMessage;
		friend class cLeakAlert;

		wxDECLARE_EVENT_TABLE();
};


