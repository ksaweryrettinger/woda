#pragma once

#include "wParameters.h"
#include "cModbusMaster.h"

class cMain;
class cModbusMaster;
class cThreadReadRTU;
class cThreadWriteRTU;

/* Looping thread, used to read Modbus holding registers */
class cThreadReadRTU : public wxThread
{
	public:
		cThreadReadRTU(cModbusMaster*);
		~cThreadReadRTU();

	protected:
		virtual ExitCode Entry();

	private:
		void UpdateModbusData(void);

	private:
		cModbusMaster* mbMaster;
		int32_t rc;
		int32_t ct;

	private: //data buffers

		bool bModbusError;
		char cNewSetting[6] = { 0 };
		uint8_t mbCoils[MB_NUM_COILS] = { 0 };
		uint16_t mbInputRegisters[MB_NUM_INPUT_REGISTERS] = { 0 };
		uint16_t mbHoldingRegisters[MB_NUM_HOLDING_REGISTERS] = { 0 };
};

/* Temporary thread, used to write new coil or holding register value */
class cThreadWriteRTU : public wxThread
{
	public:
		cThreadWriteRTU(cModbusMaster*, uint16_t*, uint8_t, int32_t, bool);
		~cThreadWriteRTU();

	protected:
		virtual ExitCode Entry();

	private:
		void UpdateModbusData(void);

	private:
		cModbusMaster* mbMaster;
		uint16_t mbData[2];
		uint8_t mbDataNum;
		int32_t mbStartAddress;
		int32_t rc;
		bool bSetCoil;
		bool bModbusError;

};

